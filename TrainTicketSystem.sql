-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 04, 2022 at 04:12 AM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `TrainTicketSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `RegisterTicket`
--

CREATE TABLE `RegisterTicket` (
  `RegisterID` int(11) NOT NULL,
  `UserID` int(60) NOT NULL,
  `EventID` int(60) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `RegisterTicket`
--

INSERT INTO `RegisterTicket` (`RegisterID`, `UserID`, `EventID`, `Date`) VALUES
(1, 3, 3, '2022-04-13'),
(2, 2, 3, '2022-04-07'),
(3, 3, 5, '2022-04-05'),
(4, 2, 3, '2022-04-19'),
(6, 8, 2, '2022-06-14');

-- --------------------------------------------------------

--
-- Table structure for table `TrainEvent`
--

CREATE TABLE `TrainEvent` (
  `EventID` int(11) NOT NULL,
  `EventOrigin` varchar(60) NOT NULL,
  `EventTerminal` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `TrainEvent`
--

INSERT INTO `TrainEvent` (`EventID`, `EventOrigin`, `EventTerminal`) VALUES
(2, 'สงขลา', 'เชียงใหม่'),
(3, 'เชียงราย', 'กรุงเทพ'),
(4, 'น่าน', 'สงขลา'),
(5, 'เชียงใหม่', 'น่าน');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `UserID` int(11) NOT NULL,
  `UserName` varchar(200) NOT NULL,
  `UserSurname` varchar(200) NOT NULL,
  `UserPhon` varchar(200) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserID`, `UserName`, `UserSurname`, `UserPhon`, `Password`, `IsAdmin`, `Name`) VALUES
(1, 'Wannisa', 'Tongkliang', '0897545325', '12345678', 0, 'wannisa'),
(2, 'Nasreeya ', 'Kullamard', '0974128965', '', 0, ''),
(3, 'Lazy', 'Loxy', '0912364785', '', 0, ''),
(4, 'Tony', 'WenWen', '0642531223', '', 0, ''),
(5, 'Juny', 'Sohny', '0794521639', '', 0, ''),
(6, 'papa', 'yaaaa', '0875421699', '', 0, ''),
(7, 'Wannisa', 'Tongkliang', '0897545325', '$2b$10$KPWgq2IlnKJdpSo8gBG7D.f3mrLb1haoxbz3KUuHKBiXlry4zSWai', 0, 'wannisa'),
(8, 'Lazy', 'Loxy', '0912364785\n', '$2b$10$nsYckSKW3L4it1jE6xSZc.UClHyjNu8QBa2AW9FxEGsDljq70xFwS', 0, 'lazyloxy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `RegisterTicket`
--
ALTER TABLE `RegisterTicket`
  ADD PRIMARY KEY (`RegisterID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `DateID` (`EventID`);

--
-- Indexes for table `TrainEvent`
--
ALTER TABLE `TrainEvent`
  ADD PRIMARY KEY (`EventID`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `RegisterTicket`
--
ALTER TABLE `RegisterTicket`
  MODIFY `RegisterID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `TrainEvent`
--
ALTER TABLE `TrainEvent`
  MODIFY `EventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `RegisterTicket`
--
ALTER TABLE `RegisterTicket`
  ADD CONSTRAINT `EventID` FOREIGN KEY (`EventID`) REFERENCES `TrainEvent` (`EventID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `UserID` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
