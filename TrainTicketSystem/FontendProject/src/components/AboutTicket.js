
function AboutTicket (porps) {


    return (
        <div>
            <h2> ชื่อ: { porps.name }</h2>
            <h2> นามสกุล: { porps.surname }</h2>
            <h3> เบอร์โทรศัพท์ { porps.telephone }</h3>
            <h3> สถานีต้นทาง { porps.origin } </h3>
            <h3> เวลาที่จอง: { porps.time }</h3>
        </div>
    );

}

export default AboutTicket;