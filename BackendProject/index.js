const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'Train',
    database :'TrainTicketSystem'

})

connection.connect();

const express  = require('express')
const app = express()
const port = 4000

/* Middleware Authenticating User Token */
function authenticateToken (req, res, next) {

    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
        if (token == null) return res.sendStatus(401)
        jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
            else {
             req.user = user
                 next()
        }
    })
}



/*
Query 1: RegisterTicket
SELECT User.UserID,User.UserName,User.UserSurname,User.UserPhon, 
TrainEvent.EventID,RegisterTicket.Date
FROM  User, RegisterTicket, TrainEvent
WHERE (RegisterTicket.UserID = User.UserID) 
AND (RegisterTicket.EventID =TrainEvent.EventID)*/

  app.get("/list_RegisterTicket" , (req, res) =>{
    let query = ` 
    SELECT User.UserID,User.UserName,User.UserSurname,User.UserPhon, 
    TrainEvent.EventID,RegisterTicket.Date
    FROM  User, RegisterTicket, TrainEvent
    WHERE (RegisterTicket.UserID = User.UserID) 
    AND (RegisterTicket.EventID =TrainEvent.EventID);` ;

     connection.query( query, (err, rows) => {
        if(err){
            res.json({
                       "status" : "400",
                       "message": "Error querying from running db"
                    })
            }else{
                res.json(rows)
             }
          });
   })


/*Query 2 : TrainEvent
SELECT User.UserID,User.UserName,User.UserSurname,User.UserPhon, 
TrainEvent.EventID,RegisterTicket.Date
FROM  User, RegisterTicket, TrainEvent
WHERE (RegisterTicket.UserID = User.UserID) 
AND (RegisterTicket.EventID =TrainEvent.EventID) AND
(TrainEvent.EventID = 1) */
app.get("/list_reg_event" , (req, res) =>{
    
    let event_id = req.query.event_id
    let query = ` 
                SELECT User.UserID,User.UserName,User.UserSurname,User.UserPhon, 
                TrainEvent.EventID,RegisterTicket.Date
                FROM  User, RegisterTicket, TrainEvent
                WHERE (RegisterTicket.UserID = User.UserID) 
                AND (RegisterTicket.EventID =TrainEvent.EventID) AND
                (TrainEvent.EventID = ${event_id});`;


                connection.query( query, (err, rows) => {
                    if(err){
                        res.json({
                                   "status" : "400",
                                   "message": "Error querying from running db"
                                })
                        }else{
                            res.json(rows)
                         }
                      });
               })
 



/*Query 3 : event register  by User ID
SELECT User.UserID, TrainEvent.EventID , RegisterTicket.DateID 
FROM User, TrainEvent, RegisterTicket
WHERE (User.UserID = RegisterTicket.UserID)AND
           (RegisterTicket.EventID = TrainEvent.EventID) AND
           (User.UserID = 2)
*/
app.get("/list_reg_userid" ,authenticateToken, (req, res) =>{
    
    let user_id = req.user.user_id
    let query = ` 
    SELECT User.UserID,User.UserName,User.UserSurname,User.UserPhon,RegisterTicket.Date
    FROM  User, RegisterTicket,TrainEvent
    WHERE (RegisterTicket.UserID = User.UserID)AND
                (RegisterTicket.EventID = TrainEvent.EventID)AND
                (User.UserID = ${user_id});`;
        
                connection.query( query, (err, rows) => {
                    if(err){
                        res.json({
                                   "status" : "400",
                                   "message": "Error querying from running db"
                                })
                        }else{
                            res.json(rows)
                         }
                      });
               })
 


/* API for Registering  to a new Ticket */
app.post("/register_RegisterTicket", authenticateToken, (req, res) => {
    let user_profile = req.user
    
    let user_id = req.user.user_id
    let event_id = req.query.event_id
    let date = req.query.date
   

    let query = `INSERT INTO RegisterTicket
                    (UserID, EventID, Date)
                    VALUES ('${user_id}','${event_id}',
                            '${date}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                        "status" : "200",
                        "message" : "Adding location succesful"
            })
        }
    });
});

/*API for Processing User  Authorization */
app.post("/login" , (req, res)=> {
    let name = req.query.name
    let user_password = req.query.password
    let query = `SELECT * FROM User WHERE Name='${name}'`
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                     "status" : "400",
                     "message": "Error querying from running db"
                  })
          }else {
             let db_password = rows[0].Password
              bcrypt.compare(user_password, db_password, (err, result) => {
                 if (result) {
                    let payload = {
                          "name" : rows[0].Name,
                          "user_id" : rows[0].UserID,
                          "IsAdmin" : rows[0].IsAdmin,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '3d'})
                    res.send(token)
                 }else { res.send("Invalid username / password") }
              })   
        }

    })
})

/* API for Registering a new User */

app.post("/register_User" , (req, res)  =>{

    let user_name = req.query.user_name
    let user_surname = req.query.user_surname
    let user_phon = req.query.user_phon
    let user_password  = req.query.user_password
    let name = req.query.name

    bcrypt.hash(user_password, SALT_ROUNDS, (err, hash) => {

        let query = `INSERT INTO User
               (UserName , UserSurname,UserPhon,Password,IsAdmin,Name )
               VALUES ('${user_name}','${user_surname}','${user_phon}',
               '${hash}', false,'${name}')`
 console.log(query)  

            connection.query( query, (err, rows) => {
            if(err){
            res.json({
                    "status" : "400",
                    "message": "Error inserting data into db"
                })
     }else{
            res.json({
                "status" : "200",
                "message": "Adding new user succesful"  
           })
         }
     });
   })
});

/* CRUD Operation for TrainTicketSystem */
app.get("/list_event" , (req, res) =>{
    let query = "SELECT * from TrainEvent";
   connection.query( query, (err, rows) => {
     if(err){
         res.json({
                    "status" : "400",
                    "message": "Error querying from running db"
                 })
         }else{
             res.json(rows)
          }
       });
})


app.post("/add_event" , (req, res) => {

    let event_origin = req.query.event_origin
    let event_terminal = req.query.event_terminal
 
    let query = ` INSERT INTO TrainEvent 
                   (EventOrigin, EventTerminal)
                   VALUES ('${event_origin}','${event_terminal}' )`
     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             res.json({
                        "status" : "400",
                        "message": "Error inserting data into db"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Adding event succesful"  
              })
           }
      });
 
  })


  app.post("/update_event" , (req, res) => {

    let event_id = req.query.event_id
    let event_origin = req.query.event_origin
    let event_terminal = req.query.event_terminal
 
    let query = `UPDATE TrainEvent SET
                   EventOrigin= '${event_origin}',
                   EventTerminal='${event_terminal}'
                   WHERE EventID=${event_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error updating record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Updating event succesful"  
              })
           }
      });
 
  })

  app.post("/delete_event" , (req, res) => {

    let event_id = req.query.event_id

    let query = `DELETE FROM TrainEvent WHERE EventID=${event_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error deleting record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Deleting record success"  
              })
           }
      });
 
  })


app.listen(port, () => {
    console.log(` Now starting Running System Backend ${port} `)
})

/*
query = "SELECT * from User";
connection.query( query, (err, rows) =>{
  if(err){
      console.log(err);
  }else{
      console.log(rows);
  
       }
});
connection.end();*/